#Crossroads of Knowledge in Early Modern England: Text Mining Tools
***********
This set of tools was designed to fascilitate an ERC-funded research project managed by CRASSH and the 
University of Cambridge's Faculty of English. The goal of this project is to uncover the interface between imaginative 
literature and epistemology in its wider sense in early modern England (1500-1700). Learn more about the Crossroads of Knowledge project [here](http://www.crassh.cam.ac.uk/programmes/crossroads). 

These tools were designed to simplify the tasks necessary for text mining the Crossroads of Knowledge corpus, which includes the plain text 
files made available by the [Early English Books Online Text Creation Partnership](http://www.textcreationpartnership.org/tcp-eebo/) (EEBO-TCP). However, these tools were built for the general
purpose of text mining, and users can apply them to any corpus of plain text files (e.g., texts from [Project Gutenberg](https://www.gutenberg.org/)).

#Getting Started 
***********
##Download Python 
Mac OS X will already have Python installed, but you may want to install the Python 2.7 version of Anaconda to ensure that you have access to the proper versions of the required packages. Python is not pre-installed on Linux or Windows. 
	
- We recommend the [64-bit installer](https://www.continuum.io/downloads) for the Python 2.7 version of Anaconda.

#####For Windows:
- Make sure Anaconda and the python scripts are included in the *Environment Variables/Path* value: 
	1. Find the *Anaconda2* folder in your directory, and right click to copy the address  
	2. Press the Windows Key, and search for "Advanced System Settings" 
	3. Click "Environment Variables" 
	4. Select the path varible, and click "Edit..."
	5. A new window will appear. Click "New," and paste the address to your Anaconda2 folder 
	6. Click "OK"
	7. Click "New" again, add the address to the "Scripts" folder (under Anaconda2), and click "OK" to save

##Install the Crossroads of Knowledge Text Mining Tools 
You'll only need to do this once.

- Open a command prompt/terminal 
- Make sure your directory is set to your home directory so you can follow along with this READme file's sample lines of code
	
	#####For Mac and Linux:
	```
	cd ~
	```
	#####For Windows:
	```
	cd %HOMEPATH%
	```

- Enter the following line of code to get a copy of the tools up and running on your local machine. You'll find a folder labelled "COK_TextMining" in your home directory (or wherever your working directrory was set to when you installed the text mining tools). 
```r
git clone https://lkersey0307@bitbucket.org/lkersey0307/cok_textmining.git
``` 
- Note that if you've never used Git before, you may need to [download the latest version](https://git-scm.com/downloads) for your operating system. Or if you're using Linux, you can install Git with a sudo command:	
```r
sudo apt install git
```

##Install Required Packages
These tools require the following packages: *sys, codecs, unicodecsv, os, glob, string, genism, six, numpy, pandas, and timeit*.
These packages should be included with the Anaconda install with the exception of *gensim*. To install *gensim*, type the following into your command line:
```r
pip install gensim
```

##Make sure your version of the tools is up-to-date with a 'git pull' 
To ensure that you have access to the most recent versions of the tools, you can run a 'git pull' command. 

- Open a terminal or command prompt 
- Change your working directory to the cok_textmining folder 
	
	#####For Mac and Linux:
	```
	cd ~/cok_textmining
	```
	#####For Windows:
	```
	cd %HOMEPATH%\cok_textmining 
	```
	
- Type the following command:
```r
git pull
```
# Table of Contents
***********
1. Process a large corpus (MakeCorpus)
2. Build and use a Term Frequency-Inverse Document Frequency model (tfidf_calculator) 
3. Shrink a corpus of vectors by preserving only a select subset of the features (vectorizer)
4. Identify the linguistic neighborhood of a word in the EEBO-TCP corpus based on its lexical context (word2vec_ebbo)
5. Authors 
6. Acknowledgements 

# Process a Large Corpus 
***********
The *makeCorpus.py* tool processes a large corpus of plain text files (e.g., the 60,284 files in the EEBO-TCP dataset) 
in such a way that reduces demands on memory. This tool automatically:

- **Tokenizes each document.** The function breaks down each document into a list of words by splitting on whitespace characters (i.e., space, tab, newline, return, formfeed). 
	
- **Coverts each word to its lowercase form** to reduce the number of features and to fascilitate searching 

- **Removes punctuation**
`! # " % $ \' & ) ( + * - , / . ; : = < ? > @ [ ] \\ _ ^ { } | ~`

- **Transforms each document into a vector.** 
	This function applies the [bag-of-words model](https://en.wikipedia.org/wiki/Bag-of-words_model), which simplifies the representation of a document by keeping only the frequencies for each unique word type. In other words, this model disregards word order and converts each document into a simple array of the document's unique words and the number of times those words appear in the document. For example, "to be or not to be" would be represented as 
	`[(to:2),(be:2),(or:1),(not:1)]` 

Users also have the option to:

- **Remove stopwords.** 
	STOPWORDS are features that a user wishes to remove from a corpus because they have little informational value. 
    Typically, stopwords are the most common words in a language and mostly include articles, pronouns, and prepositions. You can provide a path to your own stopwords list or use 
    the *makeCorpus* tool's default list, which was generated from the 500 most frequent words in the EEBO-TCP dataset. 
    The default list was then hand-curated to remove words that, though frequent, are meaningful (e.g., 'God'). It contains 231 
    words. You can scroll through the list by entering the following line of code in your command line:
	
#####For Mac and Linux:
```r
cd ~\cok_textmining\src
less stopwordslist.csv
```
#####For Windows:
```r
cd %HOMEPATH%\cok_textmining\src 
type stopwordslist.csv | more 
##Press 'Enter" to scroll down, and press 'q' to exit
```

- **Tokenize a text into larger sequences/patterns of words known as n-grams.** 
	By default, *makeCorpus* tokenizes a text into single words (a.k.a. unigrams or 1-grams). A two-word pattern is a bigram or 2-gram; a three-word pattern is a trigram or 3-gram; etc.

- **Normalize the word vectors** 
	By default, *makeCorpus* transforms documents into vectors with the raw counts for each unique word in the given 
    document. However, users can account for the size of a document by selecting the normalize option. The formula for the relative frequency of word 'w' in document 'd' is:

	`f(w,d) = (w / sum(values in d))`

##Parameters  
Shorthand | Default Value | Description 
---|---|---
-p | None | -p stands for "pathname," which specifies the location of a single plain text file or a folder of multiple plain text files. The pathname must end with a wildcard statement that represents all the plain text files in the specified folder: `*.txt` 
-g | 1 | -g stands for "gram." The documents in -p will be broken up into sequences/patterns of words equal to the size of -g. Gram must be an integer (e.g., 1,2,3). The default value of -g returns unigrams; 2 returns 2-word sequences (bigrams); 3 returns 3-word sequences (trigrams)
-r | False | -r stands for "removestopwords." If removestopwords is set to True, words that are specified as having little or no informational value are filtered from the corpus. 
-l | stopwordslist.csv | -l stands for "stopwordslist." By default, it points to a csv file with a list of high-frequency, low-content words curated from the EEBO-TCP dataset. You can use your own stopwords list by providing the path to a list of words that you would like to exclude from your calculations
-norm | False | If 'norm' is set to True, the documents will be returned as vectors of relative word frequences. Relative frequencies account for the size of the document by dividing the raw frequency of a feature by the total number of features in the document.
-n | None | -n stands for "corpusname." Choose a name for your corpus to label the output from this function. For example, if you name the corpus "tcp," then the sorted list of corpus filenames will be labelled "tcp.filenames;" the corpus will be labelled "tcp.mm;" and the dictionary will be labelled "tcp.dict." 

##Try it Yourself 

First, make sure your working directory is set to the folder with the cok_textmining tools:
#####For Mac and Linux:
```r
cd ~/cok_textmining/src
```

#####For Windows:
```
cd %HOMEPATH%\cok_textmining\src
```
To print a table of the function parameters in your command line, enter the following line of code:
```
python makeCorpus.py
```
The simplest version of this tool processes a corpus of plain text files into word vectors made of unigrams and raw counts for 
each unigram. 

#####For Mac and Linux:
```r
python makeCorpus.py -p "~/cok_textmining/data/toycorpus/*txt" -n test_corpus -g 1 -r False -norm False
```

#####For Windows:
```r
python makeCorpus.py -p "%HOMEPATH%\cok_textmining\data\toycorpus\*txt" -n test_corpus -g 1 -r False -norm False
```
##Take Advantage of the Default Values 

The values of the last three arguments are the default values for the *makeCorpus* tool, which means that the following, simpler line of code will produce the same output:

#####For Mac and Linux:
```r
python makeCorpus.py -p "~/cok_textmining/data/toycorpus/*txt" -n test_corpus 
```
#####For Windows:
```r
python makeCorpus.py -p "%HOMEPATH%\cok_textmining\data\toycorpus\*txt" -n test_corpus 
```
Note that the tool still requires a corpus name since there is no logical default for such a subjective parameter. Users should choose a name that effectively describes their corpus to ensure they can find the right data at a later time. 

##Explore the Output 

*makeCorpus* shoots back 3 different objects that will be useful for your future text mining endeavors. The first object is the corpus itself in a Matrix Market format. To explore the corpus, enter the following code:

```r
python
import gensim
from gensim import corpora
mm = corpora.MmCorpus('test_corpus.mm')
len(mm)
```
`len(mm)` returns the size of the corpus, which in this case holds 10 vectors. 
Look at the first 10 features of the first word vector:
```r
mm[0][:10]
```
This vector looks quite different compared to the original text. For starters, the words are completely absent! They've been replaced by word IDs--integers that stand in for the word types. Thus, each word vector is a list of tuples, and each tuple represents a word-frequency pair. The integers to the left of the commas are the word IDs, and the decimals to the right of the commas represent the word frequencies. To retrieve the actual words, we need to access the second object created by *myCorpus*: the dictionary. 
```r 
dict = corpora.Dictionary.load('test_corpus.dict')
dict[0]
```
A dictionary is an object with keys and corresponding values. In this case, the keys are the word IDS while the values are the word types. 
This line of code indexes the `0` key of the dictionary and returns its value, which in this case is `limited.` If you return to the first vector, you'll now be able to infer that the word 'limited' appears twice in this text. 

Finally, you'll want to know the filenames for each word vector in the corpus. The third object returned by *makeCorpus* is an alphabetical list of filenames, which corresponds to the order of the documents in the corpus. 
```r
with open('test_corpus.filenames') as f:
	filenames = [n.strip('\n')for n in f]
filenames[0]
```
Type `exit()` to exit out of python 

#Build and use a Term Frequency-Inverse Document Frequency model (Tf-IDF) 
***********
The *tfidf_calculator.py* tool ranks words (or multi-word patterns known as 'ngrams') in a subcorpus by their importance. Users provide a path to a main corpus of plain text files and a CSV file with a list of filenames for the subset of documents that constitute the subcorpus. The tool proceeds to assign a weight to each feature in the subcorpus which corresponds to how useful that feature is for distinguishing members of the subcorpus from the main corpus. These weights are calculated with the [genim module's tfidf formula](https://radimrehurek.com/gensim/models/tfidfmodel.html) which analyzes the frequency of each word or word-pattern within the context of the larger corpus. For example, if the most frequent word in the subcorpus is similarly common in the main corpus, the equation will not rank that word very high. In contrast, a word that is frequent in the subcorpus but infrequent in the main corpus will rank highly. Users can choose to recieve either an ordered list of words with the *n*-highest tf-idf scores or an ordered list of 
texts in the main corpus that are most similar to the subcorpus texts.

##Parameters  
Shorthand | Default Value | Description 
---|---|---
-m | None | -m stands for "main corpus pathname," which specifies the location of a corpus of plain text files. This main corpus must contain all the texts in the subcorpus of interest as well as documents to compare with the subcorpus. The main corpus pathname must end with a wildcard statement that represents all the plain text files in the specified folder: `*.txt` 
-s | None | -s stands for "subcorpus pathname,' which should point to a CSV file that contains the filenames (with or without the file extension) for each text in the desired subcorpus.
-r | False | -r stands for "removestopwords." If removestopwords is set to True, words that are specified as having little or no informational value are filtered from the corpus. 
-l | stopwordslist.csv | -l stands for "stopwordslist." By default, it points to a csv file with a list of high-frequency, low-content words curated from the EEBO-TCP dataset. You can use your own stopwords list by providing the path to a list of words that you would like to exclude from your calculations
-g | 1 | -g stands for "gram." The documents in -m will be broken up into sequences/patterns of words equal to the size of -g. Gram must be an integer (e.g., 1,2,3). The default value of -g returns unigrams; 2 returns 2-word sequences (bigrams); 3 returns 3-word sequences (trigrams)
-sim | False | -sim stands for "retrieve similiar documents." By default, *tfidf_calculator* returns a csv file with an ordered list of *n* words with the *n* highest tf-idf scores. If -sim is set to True, the tool will use the tf-idf scores and the [gensim similarities module](https://radimrehurek.com/gensim/similarities/docsim.html) to compute the similarity of all texts in the main corpus with the subcorpus texts. The tool then returns an ordered list of *n* documents that are most similiar to the subcorpus texts. 
-n | 100 | -n stands for "number" and specifies a threshold for the function's output. By default, it returns a list of 100 words with the highest tf-idf scores; or, if -sim is set to True, it returns the 100 most similiar documents. 
-name | None | -name stands for the name of the output of this function. 

##Try it Yourself
First, make sure your working directory is set to the folder with the cok_textmining tools:
#####For Mac and Linux:
```r
cd ~/cok_textmining/src
```

#####For Windows:
```
cd %HOMEPATH%\cok_textmining\src
```
To print a table of the function parameters in your command line, enter the following line of code:
```r
python tfidf_calculator.py
```
###Specify the subset of documents that make up your subcorpus
To identify features that distinguish a subcorpus from a larger collection of documents, the *tfidf_calculator* tool first needs a list of the documents that make up this subcorpus. You will provide this information with a CSV file that contains the filenames for each text in the desired subcorpus. For example, if the subcorpus is composed of EEBO-TCP files, the filenames will be the EEBO-TCP keys. The following is a properly formatted subcorpus list made of three EEBO-TCP files:
		
		A00001
		A00002
		A00003

Each subcorpus filename must correspond to a file in the main corpus

###Find the 10 highest tfidf unigrams in a subcorpus
#####For Mac and Linux:
```r
python tfidf_calculator.py -m "~/cok_textmining/data/toycorpus/*.txt" -s "~/cok_textmining/data/tfidf_samplelist.csv" -n 10 -name tfidf_test
```
#####For Windows:
```r
python tfidf_calculator.py -m "%HOMEPATH%\cok_textmining\data\toycorpus\*.txt" -s "%HOMEPATH%\cok_textmining\data\tfidf_samplelist.csv" -n 10 -name tfidf_test
```
###Find the document that is most similiar to a subcorpus
#####For Mac and Linux:
```r
python tfidf_calculator.py -m "~/cok_textmining/data/toycorpus/*.txt" -s "~/cok_textmining/data/tfidf_samplelist.csv" -n 2 -sim True -name tfidf_sim_test
```
#####For Windows:
```r
python tfidf_calculator.py -m "%HOMEPATH%\cok_textmining\data\toycorpus\*.txt" -s "%HOMEPATH%\cok_textmining\data\tfidf_samplelist.csv" -n 2 -sim True -name tfidf_sim_test
```
##Explore the Output
In the cok_textmining\src folder, you'll find a csv file with a label matching the name you provided following the -name argument. Note that tfidf_sim_test.csv is a list of 2 document titles. The first document is "subcorpus as single doc," which is the subcorpus texts concatenated together as a single document. It makes sense that this document would be most similar to itself, and the following text is the external document that is most similar to the subcorpus. 

#Shrink a corpus of vectors by preserving only a select subset of the features 
*********** 
The *vectorizer.py* tool transforms a single document or each document in a corpus into an *n*-dimensional vector of numerical elements that represent the frequencies of specified features. *n* is equal to the number of features of interest, which can be single words or n-grams. The *vectorizer* tool can be used in combination with the *tfidf_calculator* tool. For example, the *n*-highest-tfidf-words of a subcorpus could be the *n* features that make up your reduced vectors. 

##Parameters  
Shorthand | Default Value | Description 
---|---|---
-p | None | -p stands for "pathname," which specifies the location of a single plain text file or a folder of multiple plain text files. The pathname must end with a wildcard statement that represents all the plain text files in the specified folder: `*.txt` 
-f | None | -f stands for "feature list,' which should be a csv file that specifies the features of interest in the form of a list. 
-r | False | -r stands for "removestopwords." If removestopwords is set to True, words that are specified as having little or no informational value are filtered from the corpus. 
-l | stopwordslist.csv | -l stands for "stopwordslist." By default, it points to a csv file with a list of high-frequency, low-content words curated from the EEBO-TCP dataset. You can use your own stopwords list by providing the path to a list of words that you would like to exclude from your calculations
-norm | False | If 'norm' is set to True, the documents will be returned as vectors of relative word frequences. Relative frequencies account for the size of the document by dividing the raw frequency of a feature by the total number of features in the document.
-n | None | -n stands for "corpusname." Choose a name for your corpus to label the output from this function. For example, if you name the corpus "tcp," then the sorted list of corpus filenames will be labelled "tcp.filenames;" the corpus will be labelled "tcp.mm;" and the dictionary will be labelled "tcp.dict." 
-mm | False | By default, *vectorizer.py* returns the corpus as a csv file with a document-term matrix. If -mm is set to True, this tool returns the corpus in a .mm format, a dictionary, and an ordered list of filenames.

##Try it Yourself
First, make sure your working directory is set to the folder with the cok_textmining tools:
#####For Mac and Linux:
```r
cd ~/cok_textmining/src
```
#####For Windows:
```
cd %HOMEPATH%\cok_textmining\src
```
You can read a full list of the tool's parameters by entering the following line of code in your command line:
```r
python vectorizer.py
```
###Specify the set of features that will be preserved in your word vectors 
The *vectorizer* tool first needs a list of features that the user wishes to keep in the corpus of reduced word vectors. You will provide this information as a CSV file with all of the features of interest organized as a list. The list should start on the first row. For example, the following is a properly formatted list of 5 features:

	trade 
	commodities 
	exchanges
	merchant's 
	exchange

This CSV file must be encoded as UTF-8 
	
###Vectorize 
#####For Mac and Linux:
```r
python vectorizer.py -f "~/cok_textmining/data/vectorizer_samplefeaturelist.csv" -p "~/cok_textmining/data/toycorpus/*txt" -r False -norm False -n tcp_vectortest -mm False 
```
#####For Windows:
```r
python vectorizer.py -f "%HOMEPATH%\cok_textmining\data\vectorizer_samplefeaturelist.csv" -p "%HOMEPATH%\cok_textmining\data\toycorpus\*txt" -r False -norm False -n tcp_vectortest -mm False 
```
###Explore the Output 
By default, *vectorizer.py* returns the reduced corpus as a document-term-matrix in a .csv format. Each column represents a unique feature, and each row represents a text. 
Alternatively, users have the option of receiving the 3 objects that are also returned by the *makeCorpus* function: the corpus in a Matrix Market format, a dictionary, and an ordered list of filenames. Both forms of output can be found in the cok_textmining\src folder.

# Identify the linguistic neighborhood of a word in the EEBO-TCP corpus based on its lexical context (word2vec_ebbo)
***********
The *word2vec_eebo.py* tool takes a word or a list of words as input and returns a CSV file with each word's *n* nearest neighbors in the EEBO-TCP corpus. Users can also provide a list of subcorpus documents to compare the word's broader context with its context in a sub-domain.   

##Parameters  
Shorthand | Default Value | Description 
---|---|---
-f | None | -f stands for "feature list,' which should be a csv file that specifies the features of interest in the form of a list. 
-s | None | -s stands for "subcorpus pathname,' which should point to a CSV file that contains the filenames (with or without the file extension) for each text in the desired subcorpus. 
-n | 10 | -n stands for "number" and specified the number of nearest words that will be returned for every search term. For example, if n=10, then the spreadsheet will list the 10 nearest word vectors for each word in featurelist. 
-name | None | -n stands for "outputname." Choose a name to label the CSV file that is the output from this function. For example, if you define -name as "word2vec_test", then the spreadsheet will be labelled "word2vec_test.csv." 

## Try it Yourself
First, make sure your working directory is set to the folder with the cok_textmining tools:
#####For Mac and Linux:
```r
cd ~/cok_textmining/src
```

#####For Windows:
```
cd %HOMEPATH%\cok_textmining\src
```
To print a table of the function parameters in your command line, enter the following line of code:
```r
python word2vec_eebo.py
```
###Specify the set of features for which you want to find the lexical neighborhood  
You will provide this information as a CSV file with all of the features of interest organized as a list. The list should start on the first row. For example, the following is a properly formatted list of 5 features:

	trade 
	commodities 
	exchanges
	merchant's 
	exchange

This CSV file must be encoded as UTF-8 

###Optional: specify a subset of documents to see how the features' linguistic neighborhood differs in a subcorpus
You will specify the subcorpus with a CSV file that contains the filenames for each text in the desired subcorpus. For example, if the subcorpus is composed of EEBO-TCP files, the filenames will be the EEBO-TCP keys. The following is a properly formatted subcorpus list made of three EEBO-TCP files:
		
		A00001
		A00002
		A00003

Each subcorpus filename must correspond to a file in the main corpus

###Run Word2Vec on only the main EEBO-TCP corpus 
#####For Mac and Linux:
```r
python word2vec_eebo.py -f "~/cok_textmining/data/vectorizer_samplefeaturelist.csv" -name word2vec_allEEBO_test
```
#####For Windows:
```r
python word2vec_eebo.py -f "%HOMEPATH%\cok_textmining\data\vectorizer_samplefeaturelist.csv" -name word2vec_allEEBO_test 
```
###Compare EEBO-TCP Word2Vec results with a word vector model of a subcorpus
#####For Mac and Linux:
```r
python word2vec_eebo.py -f "~/cok_textmining/data/vectorizer_samplefeaturelist.csv" -name word2vec_subcorpus_test -s "~/cok_textmining/data/word2vec_samplesubcorpuslist.csv"
```
#####For Windows
```r
python word2vec_eebo.py -f "%HOMEPATH%\cok_textmining\data\vectorizer_samplefeaturelist.csv" -name word2vec_subcorpus_test -s "%HOMEPATH%\cok_textmining\data\word2vec_samplesubcorpuslist.csv" 
```
###Explore the Output 
*word2vec_eebo.py* returns the results of a query in .csv file. Each row represents one of the query terms from the feature list, and each column represents a word in its linguistic neighborood. This .csv file can be found in the cok_textmining\src folder.

# Authors 
***********
Lauren Kersey and Keegan Hughes 

# Acknowledgments 
***********
Special thanks to the Humanities Digital Workshop at Washington University in Saint Louis. In particular:

- Anupam Basu, Assistant Professor of English 
- Douglas Knox, Assistant Director
- Steve Pentecost, Digital Humanities Specialist 