import sys 
import config
cfg = config.cliparse.CliParse(argv = sys.argv, 
help_str="Welcome to the findsignature.py tool. \n\
\n\
", 
keys= {'corpuspath':[None,'p',True,
"\n\
           corpuspath can point to a single plain text file or a directory of multiple plain text files.\n"],
'window':[5,'w',True,
'\n\
            w must be an integer (e.g., 5, 10, 20). So, if n \n\
            is 5, the context of a keyword will be defined as the n \n\
            words on either side.\n'],
'removestopwords':[False,'r',True,
'\n\
            sdfl.\n'],
'stopwords_l':['stopwordslist.csv','l',False,'\n\
            This function comes with a default stopwords list of 231 words \n\
            curated from the 500 most frequent words in the EEBO-TCP corpus. \n\
            You can use your own stopwords list by providing the path to a \n\
            list of words that you would like to exclude from your calculations.\n'],
'f1':[None,'f1',True],
'f2':[None, 'f2',True],
'name':[None,'name',True]},summary=True).parse()
def findwordsignatures(corpuspath, f1, f2, window, name, removestopwords, stopwords_l):
	#This bit of code is useful for reading in 'utf-8' 
	reload(sys)
	sys.setdefaultencoding('utf-8')

	import gensim 
	from gensim import models
	import glob
	import codecs 
	import unicodecsv as csv 
	import os
	import pandas

	#Load the csv file with the first set of features:
	with codecs.open(f1,'rb',encoding='utf-8') as csvfile:
		reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
		records = [row for row in reader if row != " "]
	if records[0][0][0]==u'\ufeff':
		records[0][0] = records[0][0].replace(u'\ufeff','')
	records = ["".join(r) for r in records]
	print 'number of words in featureset 1:', len(records)

	#Load the csv file with the second set of features:
	with codecs.open(f2,'rb',encoding='utf-8') as csvfile2:
		reader2 = csv.reader(csvfile2, delimiter=' ', quotechar='|')
		records2 = [row for row in reader2 if row != " "]
	if records2[0][0][0]==u'\ufeff':
		records2[0][0] = records2[0][0].replace(u'\ufeff','')
	records2 = ["".join(r) for r in records2]
	print 'number of words in featureset 2:', len(records2)

	#Extract a list of all filenames in the main corpus
	if corpuspath != False:
		filenames = sorted(glob.glob(corpuspath))
		print 'number of corpus docs:', len(filenames)
	
	import string 
	punctuation = " ".join(set(string.punctuation))
	if removestopwords==True:
		with codecs.open(stopwords_l,'rb',encoding='utf-8') as stopwords:
			reader = csv.reader(stopwords, delimiter=' ', quotechar='|')
			stopwordsl = ["".join(row) for row in reader]
		print 'Stopwords:', stopwordsl
	
	def preprocess(rawstring):
		tokens = rawstring.split()
		if removestopwords==True:
			tokens_a = [t.lower().strip(punctuation)for t in tokens if t.lower() not in stopwordsl]
		else:
			tokens_a = [t.lower().strip(punctuation)for t in tokens]
		tokens_clean = [t for t in tokens_a if t.strip() > ""]
		return tokens_clean

	from six import iteritems
	class MyCorpus(object):
		def __init__(self,filenames):
			self.filenames = filenames
		def __iter__(self):
			for f in filenames:
				yield preprocess(codecs.open(f, 'r', encoding ='utf-8').read())

	corpus_memory_friendly = MyCorpus(filenames)

	results = []
	textlist_results = []
	for f in records:
		f2contextcounts = [] 
		feature_textlist = []
		textlist = {el:[] for el in records2}
		context = []
		for i,t in enumerate(corpus_memory_friendly):
			for index,word in enumerate(t):
				if word == f:
					start = index-window
					end = index+(window+1) 
					if start >= 0 & end <= len(t):
						c = t[start:end]
						for y in records2:
							if y in c and filenames[i] not in textlist[y]:
								textlist[y].append(filenames[i])
						context.append(c)

		dictionary = {}
		for item in context:
			for wd in item:
				if wd in records2 and wd in dictionary:
					dictionary[wd] = dictionary[wd]+1
				elif wd in records2 and wd not in dictionary:
					dictionary[wd] = 1
		for i in records2:
			if i in dictionary:
				f2contextcounts.append(dictionary[i])
			else:
				f2contextcounts.append(0)
		results.append(f2contextcounts)

		for i in records2:
			feature_textlist.append(tuple(textlist[i]))

		textlist_results.append(feature_textlist)

	rows = records
	columns = records2
	df = pandas.DataFrame(data=results,index=rows,columns=columns)
	df.to_csv((name + '.csv'), index = True,header = True, encoding = 'utf-8')
	df2 = pandas.DataFrame(data=textlist_results, index=rows,columns=records2)
	df2.to_csv((name + 'documentlist' + '.csv'), index=True, header=True,encoding = 'utf-8')
	print 'Congratulations! Job Complete.'

findwordsignatures(corpuspath=cfg['corpuspath'], window=cfg['window'], name=cfg['name'], removestopwords=cfg['removestopwords'],stopwords_l=cfg['stopwords_l'],f1=cfg['f1'],f2=cfg['f2'])