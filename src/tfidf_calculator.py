import sys 
import config
cfg = config.cliparse.CliParse(argv = sys.argv, 
help_str="Welcome to the tf-idf calculator. This tool ranks words (or multi-word \n\
patterns known as 'ngrams') in a subcorpus by their importance. The rankings are calculated by \n\
analyzing the frequency of each word or word-pattern within the context of \n\
the larger corpus. For example, if the most frequent word in the \n\
subcorpus is similarly common in the main corpus, the calculator will not \n\
rank that word very high. In contrast, a word that is frequent in the subcorpus \n\
but infrequent in the main corpus will rank much higher. Users can choose to recieve either an \n\
ordered list of words with the highest to the n-th highest tf-idf score or an ordered list of \n\
texts in the main corpus that are most similar to the subcorpus texts. \n\
 \n\
Single words are called unigrams or 1-grams; two-word patterns are bigrams \n\
or 2-grams; three-word patterns are trigrams or 3-grams. \n\
\n\
\n\
For detailed help, consult the README, which has \n\
the following components: \n\
\n\
What is the TFIDF calculator? \n\
How long should it take? \n\
Explanation of Terms \n\
Downloading and Installation \n\
Formatting the CSV \n\
Using NoHUP \n\
Documentation \n\
Example Test \n\
Error Messages \n\
Licensing \n\
\n\
To view the README, enter the following into the command line: \n\
    less README.md \n\
To escape the README view, press 'q' at any time.\n\
\n\
", 
keys= {'maincorpuspath':['/home/data/eebo_012617/plaintext_reg/*txt','m',True,
"\n\
            By default, maincorpuspath points to 60,284 regularized, \n\
            plain text files from EEBO-TCP. You can provide a path \n\
            to an alternative directory. That directory must contain plain \n\
            text files, and it must include all the texts in the subcorpus of interest.\n"],
'subcorpuspath':[None,'s',True,
'\n\
            The subcorpuspath should point to a CSV file that contains \n\
            the filenames for each text in the desired subcorpus. \n\
            The filenames should not include the file extension. \n\
            If the subcorpus is composed of EEBO-TCP files, the \n\
            filenames would be the EEBO-TCP keys. For example, the following \n\
            is a properly formated list of five EEBO-TCP files: \n\
            \n\
            A73575 \n\
            A07549 \n\
            A04364 \n\
            A06790 \n\
            A06791 \n\
            \n\
            Each filename must correspond to a file in the main corpus.\n'],
'gram':[1,'g',True,
'\n\
            Gram must be an integer (e.g., 1, 2, 3). 1 for unigrams, \n\
            2 for bigrams, etc.\n'],
'n':[100,'n',True,
'\n\
            n must be an integer (e.g., 100, 200, 500). So, if n \n\
            is 100, the calculator will return the top 100 grams \n\
            with the highest tf-idf scores.\n'],
'removestopwords':[False,'r',True,
'\n\
            sdfl.\n'],
'stopwords_l':['stopwordslist.csv','l',False,'\n\
            This function comes with a default stopwords list of 231 words \n\
            curated from the 500 most frequent words in the EEBO-TCP corpus. \n\
            You can use your own stopwords list by providing the path to a \n\
            list of words that you would like to exclude from your calculations.\n'],
'returnsimiliardocs':[False,'sim',False],
'name':[None,'name',True]},summary=True).parse()

def tfidf_computer(maincorpuspath, subcorpuspath, gram, n, removestopwords, stopwords_l, returnsimiliardocs,name):
	import gensim 
	from gensim import corpora
	import glob 
	import string 
	import codecs  
	import unicodecsv as csv 
	import os
	from gensim import models 
	import numpy as np

	#This bit of code is useful for reading in 'utf-8' 
	reload(sys)
	sys.setdefaultencoding('utf-8')

	#Load the csv file with the keys/filenames for the documents in the subcorpus
	with codecs.open(subcorpuspath,'rb',encoding='utf-8') as csvfile:
		reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
		records = [row for row in reader if row != " "]
	if records[0][0][0]==u'\ufeff':
		records[0][0] = records[0][0].replace(u'\ufeff','')
	records = ["".join(r) for r in records]

	#Create a list of filenames for every key in the cvs file
	subcorpus_filenames = []
	pathstart = maincorpuspath[:-5]
	print pathstart
	for r in records:
		if r[-4:] == ".txt":
			filename = pathstart + r
			subcorpus_filenames.append(filename)
		else:
			filename = pathstart + r + ".txt"
			subcorpus_filenames.append(filename)
	print 'number of subcorpus docs:', len(subcorpus_filenames)

	#Check that every filename in the subcorpus is present in the main corpus
	#Extract a list of all filenames in the main corpus
	filenames = sorted(glob.glob(maincorpuspath))
	for x in subcorpus_filenames:
		if x not in filenames:
			label = "Error:{} not in main corpus".format(x)
			print label
	# Takes a string and returns a list of tokens, converted to the lowercase and stripped of punctuation 
	print 'number of main corpus docs:',len(filenames)
	import string 
	punctuation = " ".join(set(string.punctuation))
	if removestopwords==True:
		with codecs.open(stopwords_l,'rb',encoding='utf-8') as stopwords:
			reader = csv.reader(stopwords, delimiter=' ', quotechar='|')
			stopwordsl = ["".join(row) for row in reader]
		print 'Stopwords:', stopwordsl
	
	def preprocess(rawstring):
		tokens = rawstring.split()
		if removestopwords==True:
			tokens_a = [t.lower().strip(punctuation)for t in tokens if t.lower() not in stopwordsl]
		else:
			tokens_a = [t.lower().strip(punctuation)for t in tokens]
		tokens_clean = [t for t in tokens_a if t.strip() > ""]
		return tokens_clean

	#Chunking function
	def chunks_window(l, length):
		if length == 1:
			return l 
		else:
			starts = range(0, len(l), 1)
			chunked = []
			for s in starts:
				ngram = l[s:s+length]
				if len(ngram)>1:
					glue = " "
					ngram = glue.join(ngram)
					chunked.append(ngram)
			return chunked

	#Reads in all of the subcorpus texts and combines them into a single string of text. Adds this "text" to the end of the filenames list
	subcorpus = [codecs.open(f,'r',encoding='utf-8').read() for f in subcorpus_filenames]
	single_doc = " ".join(subcorpus)
	filenames.append(single_doc)

	from six import iteritems
	dictionary = corpora.Dictionary(chunks_window(preprocess(codecs.open(fn, 'r', encoding='utf-8').read()),gram) for fn in filenames[0:-2])
	print 'len(dictionary)', len(dictionary)

	##Creates an object of class "MyCorpus" that stores each text as a vector. Each text is now a list of tuples wherein the first item is the token ID and the second item is the frequency of the token 
	class MyCorpus(object):
		def __iter__(self):
			for fn in filenames[0:-2]:
				if fn not in subcorpus_filenames:
				# assume there's one document per line, tokens separated by whitespace
					yield dictionary.doc2bow(chunks_window(preprocess(codecs.open(fn, 'r', encoding ='utf-8').read()),gram))
			yield dictionary.doc2bow(chunks_window(preprocess(single_doc),gram))
		def __getitem__(self, index):
			if filenames[index][-4:] == ".txt":
				return dictionary.doc2bow(chunks_window(preprocess(codecs.open(filenames[index], 'r', encoding='utf-8').read()),gram))
			else:
				return dictionary.doc2bow(chunks_window(preprocess(filenames[index]),gram))

	corpus_memory_friendly = MyCorpus()
	corpus_memory_friendly_filenames = []
	for fn in filenames[0:-1]:
		if fn not in subcorpus_filenames:
			corpus_memory_friendly_filenames.append(fn)
	corpus_memory_friendly_filenames.append(subcorpuspath[:-4]+'subcorpus as single doc')

	#Initialize a transformation that will convert the vectors from raw frequency counts to tf-idf scores 
	tfidf = models.TfidfModel(corpus_memory_friendly,dictionary=dictionary)
	print "tfidf is a go!"
	if returnsimiliardocs == False:
		#Compute tf-idf scores for the single "text"
		subcorpus_tfidf = tfidf[corpus_memory_friendly[-1]]

		#Sort and extract the 100 highest tf-idf scores
		subcorpus_tfidf_topwords = sorted(subcorpus_tfidf, reverse=True, key = lambda q: q[1])[:n]

		key, score = zip(*subcorpus_tfidf_topwords)
		#Replace the IDs with the corresponding token
		key = [dictionary[item]for item in key]
		#Compute the document frequency of the tokens with the highest tf-idf scores in the subcorpus
		def dfFinder(corpus):
			docfreq = {}
			for text in corpus:
				text = set(text)
				for wd in text:
					if wd in docfreq:
						docfreq[wd]+=1
					else:
						docfreq[wd] = 1
			return docfreq

		sub_corpus = [chunks_window(preprocess(codecs.open(f,'r',encoding='utf-8').read()),gram) for f in subcorpus_filenames]
		df_d = dfFinder(sub_corpus)
		df = []
		for k in key:
			df.append(df_d[k])
		score = np.array(score)
		df = np.array(df)
		df_relative = df/float(len(subcorpus_filenames))
		final_weights = score*df_relative
		subcorpus_tfidf_l = zip(key,score,df,df_relative,final_weights)

		outputpath = name + ".csv"
		with open(outputpath,'wb') as out:
			csv_out=csv.writer(out)
			csv_out.writerow(['Feature','TF-IDF Score','DF','Relative DF','Final Weights'])
			for row in subcorpus_tfidf_l:
				csv_out.writerow(row)

	else: 
		tfidf.save("tfidf.model")
		index = gensim.similarities.Similarity('tfidf.model',corpus_memory_friendly,len(dictionary))
		sims = sorted(enumerate(index[corpus_memory_friendly[-1]]),key=lambda x: -x[1])
		topsims = sims[:n]
		tcp,simscores = zip(*topsims)
		tcp = [corpus_memory_friendly_filenames[t] for t in tcp]
		topsims = zip(tcp,simscores)

		outputpath = name + ".csv"
		with open(outputpath,'wb') as out:
			csv_out=csv.writer(out)
			csv_out.writerow(['TCP Key','Similarity Score'])
			for row in topsims:
				csv_out.writerow(row)

		#gensim.corpora.MmCorpus.serialize(outputpath[:-11]+'.mm', corpus_memory_friendly)
		#with open(outputpath[:-11] + '.filenames', "w") as f:
			#f.write("\n".join(corpus_memory_friendly_filenames))

tfidf_computer(maincorpuspath=cfg['maincorpuspath'], subcorpuspath=cfg['subcorpuspath'], gram=cfg['gram'], n=cfg['n'], removestopwords=cfg['removestopwords'],stopwords_l = cfg['stopwords_l'],returnsimiliardocs=cfg['returnsimiliardocs'],name=cfg['name'])


