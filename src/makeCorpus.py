#!/usr/bin/env python 
# coding: utf-8

import sys 
import config
cfg = config.cliparse.CliParse(argv = sys.argv, 
help_str="Welcome to the corpus builder. This function takes a path to a directory of \n\
plain text files and returns a corpus in the Matrix Market format which can then be used, \n\
among other possibilities, as input for the gensim package of models. The plain text files \n\
are transformed into feature vectors (i.e., arrays of numerical elements that represent the \n\
frequency of textual features.) Users have the option of chunking the texts into multi-word \n\
patterns (known as n-grams). They can remove stopwords using a default stopwords list or a list \n\
of their own creation. The users can also choose whether the vectors are returned with relative \n\
frequencies or raw counts. The function also returns a dictionary which maps feature to their \n\
feature ID as well as a list of filenames ordered alphabetically.",
keys= {'corpuspath':[None,'p',True,
"\n\
            -p stands for corpuspath, which specifies the location of a single plain text file or a \n\
            folder of multiple plain text files. The pathname must end with a wildcard statement that \n\
            represents all the plain text files in the specified folder: *.txt\n"],
'gram':[1,'g',True,
'\n\
            Gram must be an integer (e.g., 1, 2, 3). 1 for unigrams, \n\
            2 for bigrams, etc.\n'],
'removestopwords':[False,'r',True,
'\n\
            If removestopwords is set to True, words that are specified as having \n\
            little or no informational value are filtered from the corpus. To see a \n\
            full description of the default stopwords list, look to the README file. You can \n\
            also replace the default stopwords list with a list of your own (see the \n\
         	stopwords_l parameter).\n'],
'stopwords_l':['stopwordslist.csv','l', False,
'\n\
            This function comes with a default stopwords list of 231 words \n\
            curated from the 500 most frequent words in the EEBO-TCP corpus. \n\
            You can use your own stopwords list by providing the path to a \n\
            list of words that you would like to exclude from your calculations.\n'],
'corpusname':[None,'n',True,
'\n\
			Choose a name for your corpus to label the output from this function. \n\
			For example, if you name the corpus tcp,then the following outputs will \n\
			have the following names: \n\
			\n\
			Sorted list of corpus filenames: tcp.filenames \n\
			Corpus: tcp.mm \n\
			Dictionary: tcp.dict.\n'],
'normalized':[False,'norm',True]},summary=True).parse()

def make_corpus(corpuspath,gram,removestopwords,stopwords_l, corpusname,normalized):

	import gensim 
	from gensim import corpora
	import glob 
	import string 
	import codecs  
	import unicodecsv as csv 
	import os
	from timeit import default_timer as timer

	punctuation = " ".join(set(string.punctuation))
	if removestopwords==True:
			with codecs.open(stopwords_l,'rb',encoding='utf-8') as stopwords:
				reader = csv.reader(stopwords, delimiter=' ', quotechar='|')
				stopwordsl = ["".join(row) for row in reader]
			print 'Stopwords:', stopwordsl
	def preprocess(rawstring):
		tokens = rawstring.split()
		if removestopwords==True:
			tokens_a = [t.lower().strip(punctuation)for t in tokens if t.lower() not in stopwordsl]
		else:
			tokens_a = [t.lower().strip(punctuation)for t in tokens]
		tokens_clean = [t for t in tokens_a if t.strip() > ""]
		return tokens_clean

	#Chunking function
	def chunks_window(l, length):
		if length == 1:
			return l 
		else:
			starts = range(0, len(l), 1)
			chunked = []
			for s in starts:
				ngram = l[s:s+length]
				if len(ngram)>1:
					glue = " "
					ngram = glue.join(ngram)
					chunked.append(ngram)
			return chunked

	if gram >= 2:
		print 'being memory friendly'
		from six import iteritems
		if os.path.exists(corpuspath[:-5])==True:
			filenames = sorted(glob.glob(corpuspath))
			print 'number of files:', len(filenames)
		else:
			print 'Error: pathname does not point to an existing file or directory'
		dictionary = corpora.Dictionary(chunks_window(preprocess(codecs.open(fn, 'r', encoding='utf-8').read()),gram) for fn in filenames)
		print 'len(dictionary)', len(dictionary)

		#Creates an object of class "MyCorpus" that stores each text as a vector. Each text is now a list of tuples wherein the first item is the token ID and the second item is the frequency of the token 
		class MyCorpus(object):
			def __init__(self,filepath):
				self.filenames = filenames
				self.dictionary = dictionary
			def __iter__(self):
				for fn in filenames:
					yield dictionary.doc2bow(chunks_window(preprocess(codecs.open(fn, 'r', encoding ='utf-8').read()),gram))
			def __getitem__(self, index):
				return dictionary.doc2bow(chunks_window(preprocess(codecs.open(filenames[index], 'r', encoding='utf-8').read()),gram))
		
	else:
		#Creates an class called "MyCorpus." Objects of class "MyCorpus" will store each text in a corpus as a vector, which is a list of tuples wherein the first item is the feature ID and the second item is the feature's frequency.  
		class MyCorpus(object):
			def __init__(self, filepath):
				#Create an ordered list of all filenames in the corpus
				if os.path.exists(filepath[:-5])==True:
					self.filenames = sorted(glob.glob(filepath))
					print 'number of files:', len(self.filenames)
				else:
					print 'Error: pathname does not point to an existing file or directory'
				#Create an empty dictionary to store unique features and feature IDs 
				self.dictionary = gensim.corpora.Dictionary()
			def __iter__(self):
				for fn in self.filenames:
					with codecs.open(fn, 'r', encoding='utf-8') as f:
						text = f.read()
						features = chunks_window(preprocess(text),gram)
						corpusdoc = self.dictionary.doc2bow(features,allow_update=True)
						yield corpusdoc
			def __getitem__(self, index):
				return dictionary.doc2bow(chunks_window(preprocess(codecs.open(filenames[index], 'r', encoding='utf-8').read()),gram))

	timestart = timer()
	corpus_memory_friendly = MyCorpus(corpuspath)
	print 'corpus made'
	if normalized == True and gram == 1:
		from gensim import models 
		norm = gensim.models.normmodel.NormModel(corpus_memory_friendly,norm='l1')
		print 'gensim norm model made'
	
		class MyNormCorpus(object):
			def __init__(self,dictionary):
				#Create an ordered list of all filenames in the corpus
				self.filenames = sorted(glob.glob(corpuspath))
				self.dictionary = dictionary
			def __iter__(self):
				for f in corpus_memory_friendly:
					corpusdocnorm = norm[f]
					yield corpusdocnorm

		corpus_memory_friendly_norm = MyNormCorpus(corpus_memory_friendly.dictionary)
		print 'norm corpus made'
	if normalized == True and gram >1:
		print 'normalizing the long,memory-friendly way'
		class MyNormCorpus(object):
			def __iter__(self):
				for doc in corpus_memory_friendly:
					total = 0 
					for item in doc:
						total += item[1]
					yield [(element[0],(element[1]/float(total)))for element in doc]
				
		corpus_memory_friendly_norm = MyNormCorpus()
		print 'norm corpus made'
	
	with open(corpusname + '.filenames', "w") as f:
		f.write("\n".join(corpus_memory_friendly.filenames))
	print 'filenames saved'
	
	if normalized == True:
		gensim.corpora.MmCorpus.serialize(corpusname + ".mm", corpus_memory_friendly_norm)
		print 'corpus saved'
	else:
		gensim.corpora.MmCorpus.serialize(corpusname + ".mm", corpus_memory_friendly)
		print 'corpus saved'

	corpus_memory_friendly.dictionary.save(corpusname + ".dict")
	print 'dictionary saved'

	timeend = timer()
	print("{} seconds".format(timeend - timestart))

make_corpus(corpuspath=cfg['corpuspath'], gram=cfg['gram'], removestopwords=cfg['removestopwords'],stopwords_l = cfg['stopwords_l'], corpusname=cfg['corpusname'],normalized=cfg['normalized'])

