#Define the main function. It should take in a list of words that distinguish a subcorpus/subdomain of docs and the path to a single doc or a corpus of docs to be vectorized
#!/usr/bin/env python 
# coding: utf-8
import sys 
import config
cfg = config.cliparse.CliParse(argv = sys.argv, \
help_str="Welcome to the vectorizer! This function transforms a single document or each document \n\
in a corpus into an n-dimensional vector of numerical elements that represent the frequency of \n\
specified features. n is equal to the number of features of interest, which can be single words or n-grams. They are specified \n\
as a list in the form of a csv file. \n\
\n\
", 
keys = {'featurel':[None,'f',True,
"\n\
            featurel should be a csv file with all of the features of interest organized as a list. \n\
            The list should start on the first row. For example, the following is a properly formatted \n\
            list of 5 features: \n\
            \n\
            trade \n\
            commodities \n\
            exchanges \n\
            merchant's \n\
            exchange \n\
            \n\
            The csv file should be encoded as UTF-8.\n"], 
'corpuspath':["/home/data/eebo_012617/plaintext_reg/*txt",'p',True,
'\n\
            corpuspath can point to a single plain text file or a directory of multiple plain text files. \n\
            By default, maincorpuspath points to 60,284 regularized, plain text files from EEBO-TCP.\n'],
'normalized':[False,'norm',True,
'\n\
            If normalized is set to True, the documents will be returned as vectors of relative word frequences. \n\
            Relative frequencies account for the size of the document by dividing the raw frequency of a feature \n\
            by the total number of features in the document.\n'],
'removestopwords':[False,'r',True,
'\n\
            sdfl.\n'],
'stopwords_l':['stopwordslist.csv','l',False,
'\n\
            This function comes with a default stopwords list of 231 words \n\
            curated from the 500 most frequent words in the EEBO-TCP corpus. \n\
            You can use your own stopwords list by providing the path to a \n\
            list of words that you would like to exclude from your calculations'],
'corpusname':[None,'n',True,
'\n\
			Choose a name for your corpus to label the output from this function. \n\
			For example, if you name the corpus tcp,then the following outputs will \n\
			have the following names: \n\
			\n\
			Document-Term matrix in .csv format: tcp.csv\n\
			Sorted list of corpus filenames: tcp.filenames \n\
			Corpus: tcp.mm \n\
			Dictionary: tcp.dict.\n'],
'returnmm':[False,'mm',True,
'\n\
			If mm is True, the tool will return the corpus in a .mm format, a dictionary, \n\
			and an ordered list of filenames']},summary=True).parse()

def vectorizer(featurel,corpuspath,normalized,removestopwords,stopwords_l,corpusname,returnmm):
	import unicodecsv as csv
	import gensim
	from gensim import corpora
	import codecs 
	import os
	import pandas as pd
	from timeit import default_timer as timer
	#This bit of code is useful for reading in 'utf-8' 
	reload(sys)
	sys.setdefaultencoding('utf-8')
	timestart = timer()

	#Parse the list of features 
	with codecs.open(featurel,'rb',encoding='utf-8') as csvfile:
		reader = csv.reader(csvfile, delimiter=',', quotechar='|')
		words = [row for row in reader]
	#Account for the BOM character 
	if words[0][0][0]==u'\ufeff':
		words[0][0] = words[0][0].replace(u'\ufeff','')
	print 'length of feature list:', len(words) 
	#Identify the size of the features (e.g. unigrams, bigrams, trigrams)
	words_searchable = []
	for i in words:
		words_searchable.append("".join(i))
	words_searchable = [w.lower().strip() for w in words_searchable]
	print(words_searchable)
	gram = len(words_searchable[1].split())
	print 'gram', gram

	#If the user want to shrink vectors from a pre-built corpus, load the list of filenames, the dictionary, and the corpus
	if gram == 1 and removestopwords == False and normalized == False and corpuspath[:13] == "plaintext_reg":
		print 'taking a shortcut'
		with open('tcp_allunigrams_rawcounts.filenames') as f:
			filenames = [n.strip("\n") for n in f]
		dictionary = gensim.corpora.Dictionary.load("tcp_allunigrams_rawcounts.dict")
		mm = gensim.corpora.MmCorpus('tcp_allunigrams_rawcounts.mm')
		ids = [dictionary.token2id[w] for w in words_searchable if w in dictionary.values()]
		old_id_for_new = dictionary.token2id.copy()
		dictionary.filter_tokens(good_ids=ids)
		map2compact = {old_id_for_new[v]: k for k,v in dictionary.iteritems()}
		class MyVectorizedCorpus(object):
			def __init__(self,d):
				self.dictionary = d
			def __iter__(self):
				keys = map2compact.keys()
				for doc in mm:
					yield [(map2compact[k],v) for (k,v) in doc if k in keys]
		vectorized_corpus = MyVectorizedCorpus(dictionary)
	elif gram == 2 and removestopwords == False and normalized == False and corpuspath[:13] == "plaintext_reg":
		print 'taking a shortcut'
		with open("tcp_allbigrams_rawcounts.filenames") as f:
			filenames = [n.strip("\n") for n in f]
		dictionary = gensim.corpora.Dictionary.load("tcp_allbigrams_rawcounts.dict")
		mm = gensim.corpora.MmCorpus('tcp_allbigrams_rawcounts.mm')
		ids = [dictionary.token2id[w] for w in words_searchable if w in dictionary.values()]
		old_id_for_new = dictionary.token2id.copy()
		dictionary.filter_tokens(good_ids=ids)
		map2compact = {old_id_for_new[v]: k for k,v in dictionary.iteritems()}
		class MyVectorizedCorpus(object):
			def __init__(self,d):
				self.dictionary = d
			def __iter__(self):
				keys = map2compact.keys()
				for doc in mm:
					yield [(map2compact[k],v) for (k,v) in doc if k in keys]
		vectorized_corpus = MyVectorizedCorpus(dictionary)
	elif gram == 3 and removestopwords == False and normalized == False and corpuspath[:13] == "plaintext_reg":
		print 'taking a shortcut'
		with open("tcp_trigrams.filenames") as f:
			filenames = [n.strip("\n") for n in f]
		dictionary = gensim.corpora.Dictionary.load("tcp_trigrams.dict")
		mm = gensim.corpora.MmCorpus('tcp_trigrams.mm')
		ids = [dictionary.token2id[w] for w in words_searchable if w in dictionary.values()]
		old_id_for_new = dictionary.token2id.copy()
		dictionary.filter_tokens(good_ids=ids)
		map2compact = {old_id_for_new[v]: k for k,v in dictionary.iteritems()}
		class MyVectorizedCorpus(object):
			def __init__(self,d):
				self.dictionary = d
			def __iter__(self):
				keys = map2compact.keys()
				for doc in mm:
					yield [(map2compact[k],v) for (k,v) in doc if k in keys]
		vectorized_corpus = MyVectorizedCorpus(dictionary)
	elif gram == 1 and removestopwords == False and normalized == True and corpuspath[:13] == "plaintext_reg":
		print 'taking a shortcut'
		with open("tcp_allunigrams_relativefreqs.filenames") as f:
			filenames = [n.strip("\n") for n in f]
		dictionary = gensim.corpora.Dictionary.load("tcp_allunigrams_relativefreqs.dict")
		mm = gensim.corpora.MmCorpus('tcp_allunigrams_relativefreqs.mm')
		ids = [dictionary.token2id[w] for w in words_searchable if w in dictionary.values()]
		old_id_for_new = dictionary.token2id.copy()
		dictionary.filter_tokens(good_ids=ids)
		map2compact = {old_id_for_new[v]: k for k,v in dictionary.iteritems()}
		class MyVectorizedCorpus(object):
			def __init__(self,d):
				self.dictionary = d
			def __iter__(self):
				keys = map2compact.keys()
				for doc in mm:
					yield [(map2compact[k],v) for (k,v) in doc if k in keys]
		vectorized_corpus = MyVectorizedCorpus(dictionary)
	elif gram == 2 and removestopwords == False and normalized == True and corpuspath[:13] == "plaintext_reg":
		print 'taking a shortcut'
		with open("tcp_allbigrams_relfreqs.filenames") as f:
			filenames = [n.strip("\n") for n in f]
		dictionary = gensim.corpora.Dictionary.load("tcp_allbigrams_relfreqs.dict")
		mm = gensim.corpora.MmCorpus('tcp_allbigrams_relfreqs.mm')
		ids = [dictionary.token2id[w] for w in words_searchable if w in dictionary.values()]
		old_id_for_new = dictionary.token2id.copy()
		dictionary.filter_tokens(good_ids=ids)
		map2compact = {old_id_for_new[v]: k for k,v in dictionary.iteritems()}
		class MyVectorizedCorpus(object):
			def __init__(self,d):
				self.dictionary = d
			def __iter__(self):
				keys = map2compact.keys()
				for doc in mm:
					yield [(map2compact[k],v) for (k,v) in doc if k in keys]
		vectorized_corpus = MyVectorizedCorpus(dictionary)
	elif gram == 3 and removestopwords == False and normalized == True and corpuspath[:13] == "plaintext_reg":
		print 'taking a shortcut'
		with open("tcp_alltrigrams_relativefreqs.filenames") as f:
			filenames = [n.strip("\n") for n in f]
		dictionary = gensim.corpora.Dictionary.load("tcp_alltrigrams_relativefreqs.dict")
		mm = gensim.corpora.MmCorpus('tcp_alltrigrams_relativefreqs.mm')
		ids = [dictionary.token2id[w] for w in words_searchable if w in dictionary.values()]
		old_id_for_new = dictionary.token2id.copy()
		dictionary.filter_tokens(good_ids=ids)
		map2compact = {old_id_for_new[v]: k for k,v in dictionary.iteritems()}
		class MyVectorizedCorpus(object):
			def __init__(self,d):
				self.dictionary = d
			def __iter__(self):
				keys = map2compact.keys()
				for doc in mm:
					yield [(map2compact[k],v) for (k,v) in doc if k in keys]
		vectorized_corpus = MyVectorizedCorpus(dictionary)
	elif gram == 1 and removestopwords == True and normalized == False and corpuspath[:13] == "plaintext_reg" and stopwords_l == 'stopwordslist.csv':
		print 'taking a shortcut'
		with open("tcp_unigrams_nostpwrds_rawcounts.filenames") as f:
			filenames = [n.strip("\n") for n in f]
		dictionary = gensim.corpora.Dictionary.load("tcp_unigrams_nostpwrds_rawcounts.dict")
		mm = gensim.corpora.MmCorpus('tcp_unigrams_nostpwrds_rawcounts.mm')
		ids = [dictionary.token2id[w] for w in words_searchable if w in dictionary.values()]
		old_id_for_new = dictionary.token2id.copy()
		dictionary.filter_tokens(good_ids=ids)
		map2compact = {old_id_for_new[v]: k for k,v in dictionary.iteritems()}
		class MyVectorizedCorpus(object):
			def __init__(self,d):
				self.dictionary = d
			def __iter__(self):
				keys = map2compact.keys()
				for doc in mm:
					yield [(map2compact[k],v) for (k,v) in doc if k in keys]
		vectorized_corpus = MyVectorizedCorpus(dictionary)
	elif gram == 2 and removestopwords == True and normalized == False and corpuspath[:13] == "plaintext_reg" and stopwords_l == 'stopwordslist.csv':
		print 'taking a shortcut'
		with open("tcp_bigrams_nostpwrds_rawcounts.filenames") as f:
			filenames = [n.strip("\n") for n in f]
		dictionary = gensim.corpora.Dictionary.load("tcp_bigrams_nostpwrds_rawcounts.dict")
		mm = gensim.corpora.MmCorpus('tcp_bigrams_nostpwrds_rawcounts.mm')
		ids = [dictionary.token2id[w] for w in words_searchable if w in dictionary.values()]
		old_id_for_new = dictionary.token2id.copy()
		dictionary.filter_tokens(good_ids=ids)
		map2compact = {old_id_for_new[v]: k for k,v in dictionary.iteritems()}
		class MyVectorizedCorpus(object):
			def __init__(self,d):
				self.dictionary = d
			def __iter__(self):
				keys = map2compact.keys()
				for doc in mm:
					yield [(map2compact[k],v) for (k,v) in doc if k in keys]
		vectorized_corpus = MyVectorizedCorpus(dictionary)
	elif gram == 3 and removestopwords == True and normalized == False and corpuspath[:13] == "plaintext_reg" and stopwords_l == 'stopwordslist.csv':
		print 'taking a shortcut'
		with open("tcp_trigrams_nostpwrds_rawcounts.filenames") as f:
			filenames = [n.strip("\n") for n in f]
		dictionary = gensim.corpora.Dictionary.load("tcp_trigrams_nostpwrds_rawcounts.dict")
		mm = gensim.corpora.MmCorpus('tcp_trigrams_nostpwrds_rawcounts.mm')
		ids = [dictionary.token2id[w] for w in words_searchable if w in dictionary.values()]
		old_id_for_new = dictionary.token2id.copy()
		dictionary.filter_tokens(good_ids=ids)
		map2compact = {old_id_for_new[v]: k for k,v in dictionary.iteritems()}
		class MyVectorizedCorpus(object):
			def __init__(self,d):
				self.dictionary = d
			def __iter__(self):
				keys = map2compact.keys()
				for doc in mm:
					yield [(map2compact[k],v) for (k,v) in doc if k in keys]
		vectorized_corpus = MyVectorizedCorpus(dictionary)
	elif gram == 1 and removestopwords == True and normalized == True and corpuspath[:13] == "plaintext_reg" and stopwords_l == 'stopwordslist.csv':
		print 'taking a shortcut'
		with open("tcp_unigrams_nostpwrds_relativefreqs.filenames") as f:
			filenames = [n.strip("\n") for n in f]
		dictionary = gensim.corpora.Dictionary.load("tcp_unigrams_nostpwrds_relativefreqs.dict")
		mm = gensim.corpora.MmCorpus('tcp_unigrams_nostpwrds_relativefreqs.mm')
		ids = [dictionary.token2id[w] for w in words_searchable if w in dictionary.values()]
		old_id_for_new = dictionary.token2id.copy()
		dictionary.filter_tokens(good_ids=ids)
		map2compact = {old_id_for_new[v]: k for k,v in dictionary.iteritems()}
		class MyVectorizedCorpus(object):
			def __init__(self,d):
				self.dictionary = d
			def __iter__(self):
				keys = map2compact.keys()
				for doc in mm:
					yield [(map2compact[k],v) for (k,v) in doc if k in keys]
		vectorized_corpus = MyVectorizedCorpus(dictionary)
	elif gram == 2 and removestopwords == True and normalized == True and corpuspath[:13] == "plaintext_reg" and stopwords_l == 'stopwordslist.csv':
		print 'taking a shortcut'
		with open("tcp_bigrams_nostpwrds_relfreqs.filenames") as f:
			filenames = [n.strip("\n") for n in f]
		dictionary = gensim.corpora.Dictionary.load("tcp_bigrams_nostpwrds_relfreqs.dict")
		mm = gensim.corpora.MmCorpus('tcp_bigrams_nostpwrds_relfreqs.mm')
		ids = [dictionary.token2id[w] for w in words_searchable if w in dictionary.values()]
		old_id_for_new = dictionary.token2id.copy()
		dictionary.filter_tokens(good_ids=ids)
		map2compact = {old_id_for_new[v]: k for k,v in dictionary.iteritems()}
		class MyVectorizedCorpus(object):
			def __init__(self,d):
				self.dictionary = d
			def __iter__(self):
				keys = map2compact.keys()
				for doc in mm:
					yield [(map2compact[k],v) for (k,v) in doc if k in keys]
		vectorized_corpus = MyVectorizedCorpus(dictionary)
	elif gram == 3 and removestopwords == True and normalized == True and corpuspath[:13] == "plaintext_reg" and stopwords_l == 'stopwordslist.csv':
		print 'taking a shortcut'
		with open("tcp_trigrams_nostpwrds_relativefreqs.filenames") as f:
			filenames = [n.strip("\n") for n in f]
		dictionary = gensim.corpora.Dictionary.load("tcp_trigrams_nostpwrds_relativefreqs.dict")
		mm = gensim.corpora.MmCorpus('tcp_trigrams_nostpwrds_relativefreqs.mm')
		ids = [dictionary.token2id[w] for w in words_searchable if w in dictionary.values()]
		old_id_for_new = dictionary.token2id.copy()
		dictionary.filter_tokens(good_ids=ids)
		map2compact = {old_id_for_new[v]: k for k,v in dictionary.iteritems()}
		class MyVectorizedCorpus(object):
			def __init__(self,d):
				self.dictionary = d
			def __iter__(self):
				keys = map2compact.keys()
				for doc in mm:
					yield [(map2compact[k],v) for (k,v) in doc if k in keys]
		vectorized_corpus = MyVectorizedCorpus(dictionary)

	else:
		print 'taking the long way...'
		import string
		punctuation = " ".join(set(string.punctuation))
		if removestopwords==True:
			with codecs.open(stopwords_l,'rb',encoding='utf-8') as stopwords:
				reader = csv.reader(stopwords, delimiter=' ', quotechar='|')
				stopwordsl = ["".join(row) for row in reader]
			print 'Stopwords:', stopwordsl

		def preprocess(rawstring):
			tokens = rawstring.split()
			if removestopwords==True:
				tokens_a = [t.lower().strip(punctuation)for t in tokens if t.lower() not in stopwordsl]
			else:
				tokens_a = [t.lower().strip(punctuation)for t in tokens]
			tokens_clean = [t for t in tokens_a if t.strip() > ""]
			return tokens_clean
		
		def chunks_window(l, length):
			if length == 1:
				return l 
			else:
				starts = range(0, len(l), 1)
				chunked = []
				for s in starts:
					ngram = l[s:s+length]
					if len(ngram)>1:
						glue = " "
						ngram = glue.join(ngram)
						chunked.append(ngram)
				return chunked
		import glob
		import copy
		filenames = sorted(glob.glob(corpuspath))
		from six import iteritems
		dictionary = corpora.Dictionary(chunks_window(preprocess(codecs.open(fn, 'r', encoding='utf-8').read()),gram) for fn in filenames)
		print 'length of dictionary:',len(dictionary)
		class MyCorpus(object):
			def __init__(self,filenames,dictionary):
				self.filenames = filenames
				self.dictionary = copy.deepcopy(dictionary)
			def __iter__(self):
				for fn in self.filenames:
					yield self.dictionary.doc2bow(chunks_window(preprocess(codecs.open(fn, 'r', encoding ='utf-8').read()),gram))
			def __getitem__(self, index):
				return self.dictionary.doc2bow(chunks_window(preprocess(codecs.open(self.filenames[index], 'r', encoding='utf-8').read()),gram))
		mm = MyCorpus(filenames, dictionary)

		if normalized == False:
			dictionary = mm.dictionary	
			print 'dict len',len(dictionary)
			ids = [dictionary.token2id[w] for w in words_searchable if w in dictionary.values()]
			
			class MyVectorizedCorpus(object):
				def __init__(self,mm,ids):
					self.mm = mm
					self.dictionary = copy.deepcopy(self.mm.dictionary)
					old_id_for_new = self.dictionary.token2id.copy()
					self.dictionary.filter_tokens(good_ids=ids)
					self.map2compact = {old_id_for_new[v]: k for k,v in self.dictionary.iteritems()}
				def __iter__(self):
					keys = self.map2compact.keys()
					for doc in self.mm:
						newdoc= [(self.map2compact[k],v) for (k,v) in doc if k in keys]
						yield newdoc

			vectorized_corpus = MyVectorizedCorpus(mm,ids)
		
		else:
			from gensim import models 
			norm = gensim.models.normmodel.NormModel(mm,norm='l1')
			
			class MyNormCorpus(object):
				def __init__(self,dictionary):
					self.dictionary = copy.deepcopy(dictionary)
				def __iter__(self):
					for f in mm:
						corpusdocnorm = norm[f]
						yield corpusdocnorm
			nmm = MyNormCorpus(dictionary)
	
			dictionary = nmm.dictionary	
			ids = [dictionary.token2id[w] for w in words_searchable if w in dictionary.values()]

			class MyVectorizedCorpus(object):
				def __init__(self,mm,ids):
					self.mm = mm
					self.dictionary = copy.deepcopy(self.mm.dictionary)
					old_id_for_new = self.dictionary.token2id.copy()
					self.dictionary.filter_tokens(good_ids=ids)
					self.map2compact = {old_id_for_new[v]: k for k,v in self.dictionary.iteritems()}
				def __iter__(self):
					keys = self.map2compact.keys()
					for doc in self.mm:
						newdoc= [(self.map2compact[k],v) for (k,v) in doc if k in keys]
						yield newdoc

			vectorized_corpus = MyVectorizedCorpus(nmm,ids)
	
	if returnmm == True:
		gensim.corpora.MmCorpus.serialize(corpusname + '.mm', vectorized_corpus)
		with open(corpusname + '.filenames', "w") as f:
			f.write("\n".join(filenames))
		vectorized_corpus.dictionary.save(corpusname + ".dict")
	else:
		numpy_matrix = gensim.matutils.corpus2dense(vectorized_corpus, num_docs= len(filenames), num_terms=len(vectorized_corpus.dictionary))
		numpy_matrix = numpy_matrix.transpose()
		titles = [os.path.split(f)[1][:-4] for f in filenames]
		#Convert the matrix into a dataframe with the feature list and document titles 
		df = pd.DataFrame(numpy_matrix,columns=sorted(vectorized_corpus.dictionary.items()),index=titles)
		df.to_csv((corpusname + '.csv'), index = True,header = True, encoding = 'utf-8')
	timeend = timer()
	print (timeend-timestart)

vectorizer(featurel=cfg['featurel'],corpuspath=cfg['corpuspath'],normalized=cfg['normalized'],removestopwords=cfg['removestopwords'],stopwords_l=cfg['stopwords_l'],corpusname=cfg['corpusname'],returnmm=cfg['returnmm'])