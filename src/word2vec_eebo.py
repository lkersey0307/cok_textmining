import sys 
import config
cfg = config.cliparse.CliParse(argv = sys.argv, 
help_str="Welcome to word2vec_eebo. \n\
 \n\
", 
keys= {'subcorpuspath':[False,'s',False,
'\n\
            The subcorpuspath should point to a CSV file that contains \n\
            the filenames for each text in the desired subcorpus. \n\
            The filenames should not include the file extension. \n\
            If the subcorpus is composed of EEBO-TCP files, the \n\
            filenames would be the EEBO-TCP keys. For example, the following \n\
            is a properly formated list of five EEBO-TCP files: \n\
            \n\
            A73575 \n\
            A07549 \n\
            A04364 \n\
            A06790 \n\
            A06791 \n\
            \n\
            Each filename must correspond to a file in the main EEBO-TCP corpus.\n'],
'featurelist':[None,'f',True,
'\n\
            featurelist should be a csv file with all of the features of interest organized as a list. \n\
            The list should start on the first row. For example, the following is a properly formatted \n\
            list of 5 features: \n\
            \n\
            trade \n\
            commodities \n\
            exchanges \n\
            merchants \n\
            exchange \n\
            \n\
            The csv file should be encoded as UTF-8.\n'], 
'featurelist2':[False,'ff',False,
'\n\
			A second list of features.\n'],
'n':[10,'n',True,
'\n\
            n must be an integer (e.g., 5, 10, 20). So, if n \n\
            is 10, the calculator will return the top 10 most \n\
            similar words for each feature.\n'],
'name':[None,'name',True]},summary=True).parse()

def word2vec_eebo(subcorpuspath, featurelist, n, name,featurelist2):
	#This bit of code is useful for reading in 'utf-8' 
	reload(sys)
	sys.setdefaultencoding('utf-8')

	import gensim 
	from gensim import models
	import glob
	import codecs 
	import unicodecsv as csv 
	import os
	import pandas

	#Load the csv file with the features
	with codecs.open(featurelist,'rb',encoding='utf-8') as csvfile:
		reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
		records = [row for row in reader if row != " "]
	if records[0][0][0]==u'\ufeff':
		records[0][0] = records[0][0].replace(u'\ufeff','')
	records = ["".join(r) for r in records]
	print 'number of words:', len(records)

	if subcorpuspath != False: 
		with codecs.open(subcorpuspath,'rb',encoding='utf-8') as csvfile_subcorpus:
			reader_subcorpus = csv.reader(csvfile_subcorpus, delimiter=' ', quotechar='|')
			subcorpusrecords = [row for row in reader_subcorpus if row != " "]
		if subcorpusrecords[0][0][0]==u'\ufeff':
			subcorpusrecords[0][0] = subcorpusrecords[0][0].replace(u'\ufeff','')
		subcorpusrecords = ["".join(r) for r in subcorpusrecords]
		print 'number of subcorpus texts', len(subcorpusrecords)
		print subcorpusrecords

		#Create a list of filenames for every key in the cvs file
		subcorpus_filenames = []
		pathstart = "plaintext_reg_bysent"
		for r in subcorpusrecords:
			if r[-4:] == ".txt":
				filename = os.path.join(pathstart, r)
				subcorpus_filenames.append(filename)
			else:
				r = r+".txt"
				filename = os.path.join(pathstart, r)
				subcorpus_filenames.append(filename)

	#Import the word vector model for all of EEBO-TCP 
	model = gensim.models.word2vec.Word2Vec.load('word2vecmodel')
	#Find most similar words for the features in the feature list 
	word_neighborhood_mainmodel = [zip(*model.wv.most_similar(r,topn=n))[0] for r in records]
	if subcorpuspath != False:
		#Build the word vector model for the subcorpus
		from six import iteritems 
		class my_serialized_sentences(object):
			def __init__(self,filenames):
				self.filenames = filenames
			def __iter__(self):
				for fn in self.filenames:
					for sentence in gensim.models.word2vec.LineSentence(fn):
						yield sentence 

		sentences_serialized = my_serialized_sentences(subcorpus_filenames)
		subcorpusmodel = gensim.models.word2vec.Word2Vec(sentences_serialized,size=100,window=5,min_count=5,workers=4)
	
		#Find most similar words for the features in the feature list
		word_neighborhood_submodel = []
		for r in records:
			if r in subcorpusmodel.wv.vocab:
				word_neighborhood_submodel.append(zip(*subcorpusmodel.wv.most_similar(r,topn=n))[0])
			else:
				warning_t = ['Word not in subcorpus vocabulary or has a frequency <5']
				for i in range(0,(n-1)):
					warning_t.append('na')
				warning_t = tuple(warning_t)
				word_neighborhood_submodel.append(warning_t)
		for index,t in enumerate(word_neighborhood_mainmodel):
			word_neighborhood_mainmodel[index] = t + word_neighborhood_submodel[index]
		columntitles = ['eebo-tcp model_neighboring word' + " " + str(i) for i in range(1,n+1)]
		for i in range(1,n+1):
			columntitles.append('subcorpus model_neighboring word' + " " + str(i))
	else:
		columntitles = ['eebo-tcp model_neighboring word' + " " + str(i) for i in range(1,n+1)]
	df = pandas.DataFrame(data=word_neighborhood_mainmodel,index=records,columns=columntitles)
	df.to_csv((name + '.csv'), index = True,header = True, encoding = 'utf-8')

	if featurelist2 != False:
		with codecs.open(featurelist2,'rb',encoding='utf-8') as csvfile2:
			reader2 = csv.reader(csvfile2, delimiter=' ', quotechar='|')
			records2 = [row for row in reader2 if row != " "]
		if records2[0][0][0]==u'\ufeff':
			records2[0][0] = records2[0][0].replace(u'\ufeff','')
		records2 = ["".join(r) for r in records2]
		print 'number of words in second feature list:', len(records2)

		sims = []  
		for w in records2: 
			sims.append([model.wv.similarity(w,records[i]) for i,v in enumerate(records)])
		rows2 = records2
		columns2 = records
		#rows2.append('average similarity between f1 and f2')
		df2 = pandas.DataFrame(data=sims,index=rows2,columns=columns2)
		df2.to_csv((name + 'neighborhood_distances' + '.csv'), index = True,header = True, encoding = 'utf-8')

	print 'Congratulations! Job Complete.'
word2vec_eebo(subcorpuspath=cfg['subcorpuspath'],featurelist=cfg['featurelist'],n=cfg['n'],name=cfg['name'],featurelist2=cfg['featurelist2'])

